# stabilis-ntp

Simple docker image with ntp (chrony) server, which serves current server local time.

We use it at Stabilis to synchronize peer times to our server. 

It's up to the server to have a proper time, everything should work as expected as long as peers and server has same time, no matter if this is a real time.
